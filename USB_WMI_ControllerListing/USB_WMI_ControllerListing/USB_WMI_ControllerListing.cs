﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Management;
using Microsoft.Management.Infrastructure;


namespace USB_WMI_ControllerListing
{
    public class USB_WMI_ControllerListing
    {
        private static int GetNumberUSBControllerDevices()
        {
            ManagementObjectSearcher searcher = new ManagementObjectSearcher("root\\CIMV2", "SELECT * FROM Win32_USBControllerDevice");
            return searcher.Get().Count;
        }


        public static string[] GetDeviceIds()
        {
            string[] deviceIDArray = new string[GetNumberUSBControllerDevices()];
            ManagementObjectSearcher searcher = new ManagementObjectSearcher("root\\CIMV2", "SELECT * FROM Win32_USBControllerDevice");
            int count = 0;
            foreach (ManagementObject queryObj in searcher.Get())
            {
                ManagementObject oDependent = new ManagementObject();
                ManagementPath ObjectPath = new ManagementPath((String)queryObj["Dependent"]);
                oDependent.Path = ObjectPath;
                oDependent.Get();

                //Console.WriteLine("Device ID: " + oDependent["DeviceID"]);
                //Console.WriteLine("Description: " + oDependent["Description"]);
                deviceIDArray[count] = oDependent["DeviceID"].ToString();
                count++;
            }
            return deviceIDArray;

        }

        public static string [] GetDescriptions()
        {
            string[] descriptionArray = new string[GetNumberUSBControllerDevices()];
            ManagementObjectSearcher searcher = new ManagementObjectSearcher("root\\CIMV2", "SELECT * FROM Win32_USBControllerDevice");
            int count = 0;
            foreach (ManagementObject queryObj in searcher.Get())
            {
                ManagementObject oDependent = new ManagementObject();
                ManagementPath ObjectPath = new ManagementPath((String)queryObj["Dependent"]);
                oDependent.Path = ObjectPath;
                oDependent.Get();

                descriptionArray[count] = oDependent["Description"].ToString();
                count++;
            }
            return descriptionArray;
        }
    }
}
