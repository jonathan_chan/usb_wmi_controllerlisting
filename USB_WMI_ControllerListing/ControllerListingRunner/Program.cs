﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using USB_WMI_ControllerListing;

namespace ControllerListingRunner
{
    class Program
    {
        static void Main(string[] args)
        {
            //USB_WMI_ControllerListing.USB_WMI_ControllerListing.GetUsingCIM();
            //USB_WMI_ControllerListing.USB_WMI_ControllerListing.GetDeviceDescriptionsAndIds();
            string [] descriptions = USB_WMI_ControllerListing.USB_WMI_ControllerListing.GetDescriptions();
            string [] deviceIds = USB_WMI_ControllerListing.USB_WMI_ControllerListing.GetDeviceIds();

            PrintKeyInfo(descriptions, deviceIds);
        }

        private static void PrintKeyInfo(string[]descriptions, string[] deviceIds)
        {
            if (descriptions.Length == deviceIds.Length)
            {
                for (int i=0; i< descriptions.Length; i++)
                {
                    Console.WriteLine("CONTROLLER " + i);
                    Console.WriteLine(descriptions[i]);
                    Console.WriteLine(deviceIds[i]);
                }
            }
        }
    }
}
